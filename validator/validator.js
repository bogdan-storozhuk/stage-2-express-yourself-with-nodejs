class Validator {
    static validateUser(user) {
        const {
            _id,
            name,
            health,
            attack,
            defense,
            source
        } = user;
        if (!_id) {
            return {
                isValid: false,
                message: "id is not correct"
            };
        }

        if (isNaN(health)) {
            return {
                isValid: false,
                message: "health is not correct"
            };
        }

        if (isNaN(attack)) {
            return {
                isValid: false,
                message: "attack is not correct"
            };
        }

        if (isNaN(defense)) {
            return {
                isValid: false,
                message: "defense is not correct"
            };
        }

        if (!name) {
            return {
                isValid: false,
                message: "name is not correct"
            };
        }

        return {
            isValid: true,
            message: null
        };
    }
}

module.exports = {
    Validator,
};